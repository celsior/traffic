#!/usr/bin/python

import sys
import numpy
from numpy.random import exponential, normal

t = 0.0
dt = 0.1
avg_v = 0
N = 0


class Lights:
    # phases: a dictionary of phases' lengths in seconds
    # x: the place of the light on the road
    # phase: the initial phase
    def __init__(self, phases, x, phase):
        self.phases = phases
        self.x = x
        self.time = 0.0
        self.phase = phase

    def step(self, dt):
        self.time += dt
        if self.time >= self.phases[self.phase]:
            self.time = 0.0

            if self.phase == 'r':
                self.phase = 'm'
            elif self.phase == 'm':
                self.phase = 'g'
            elif self.phase == 'g':
                self.phase = 'y'
            elif self.phase == 'y':
                self.phase = 'r'


class Car:
    # x: the initial place
    # v: the initial speed
    # t0: the time of car's appearing (to calculate its average speed
    #     at the end)
    def __init__(self, x, v, t0):
        self.x = x
        self.v = v
        self.a = 0.0
        self.lights = None
        self.prev_car = None
        self.t0 = t0
        self.prev_dist = 100000.0

    def step(self, dt):
        if self.prev_car and self.prev_car.x - self.x < 0:
            raise Exception("Crash!")

        dist = 100000.0
        if self.lights:
            dist = self.lights.x - self.x
        if self.prev_car:
            dist = min(dist,  self.prev_car.x - self.x - 6)
        if (dist < 0):
            dist = 1e-8

        dx = dist - self.prev_dist
        dxdt = dx/dt

        self.a = 4.0 if self.v < 18 else (18-self.v)/2
        if dist < 50:
            if dxdt > 0:
                if dist > 15:
                    self.a = dxdt/dt/2 * 2
                elif dist > 10:
                    self.a = dxdt/dt/2
                else:
                    self.a = dxdt/dt/2 * 0.2
            else:
                if dist < 10:
                    self.a = - self.v**2 / (2 * dist)
                elif dist < 6:
                    self.a = dxdt/dt/2 / 10
        self.a = min(self.a, 4.0)

        self.prev_dist = dist

        self.v += self.a*dt
        if self.v < 0:
            self.v = 0
        self.x += self.v*dt
        if self.v < 0.1:
            self.v = 0.0
            self.a = 0.0

class Input:
    def __init__(self, cars, interval, x, v0):
        # cars: the array of cars on the road
        # interval: the average interval between cars in seconds
        # x: the place of the cars source
        # v0: the initial cars' speed
        self.cars = cars
        self.interval = interval
        self.time = 0.0
        self.next_time = exponential(self.interval)
        self.x = x
        self.v0 = v0
        self.waiting = 0

    def step(self, dt):
        self.time += dt
        if self.time > self.next_time:
            self.time = 0
            self.next_time = exponential(self.interval)
            self.waiting += 1

        if self.waiting == 0:
            return

        if self.cars and self.cars[-1].x - self.x < 10:
            return

        car = Car(self.x, self.v0, t)
        car.prev_car = self.cars[-1] if self.cars else None
        self.cars.append(car)
        self.waiting -= 1



import matplotlib.pyplot as plt
import matplotlib.animation as animation

fig = plt.figure()
ax = fig.add_subplot(111, autoscale_on=False,
                     xlim=(-1020, 20), ylim=(-1, 1))
pcars, = ax.plot([],[], 'bo', ms=6)
ax.grid()

lights = [Lights({'r': 16, 'm': 2, 'g': 16, 'y' : 2}, -50.0, 'r')]

for d in range(-150,-950,-100):
    lights.append(Lights({'r': 3, 'm': 2, 'g': 6.5, 'y' : 2}, d, 'g'))


for l in lights:
    l.img, = ax.plot([],[], 'bo', ms=10)

cars = []
input = Input(cars, 2.2, -1000.0, 2.0)

def step(i):
    global t
    t += dt

    for l in lights:
        l.step(dt)
        l.img.set_data([l.x], [0.1])
        l.img.set_markerfacecolor(l.phase)

    input.step(dt)

    while cars and cars[0].x > 0:
        c = cars.pop(0)
        print t, c.t0
    if cars:
        cars[0].prev_car = None

    nL = len(lights)-1
    nC = len(cars)-1
    while nC >= 0:
        while nL >= 0 and (cars[nC].x > lights[nL].x+1 or lights[nL].phase not in ['r','y']):
            nL -= 1

        cars[nC].lights = lights[nL] if nL >= 0 else None
        nC -= 1

    x = []
    for c in cars:
        c.step(dt)
        x.append(c.x)
    pcars.set_data(x, [0]*len(cars))

    return ()

for i in range(100000):
    step(i)

# ani = animation.FuncAnimation(fig, step, frames=60000,
#                               interval=1, blit=True)
# plt.show()
