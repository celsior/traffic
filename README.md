TRAFFIC LIGHTS TO REDUCE TRAFFIC JAM
(a quick and dirty illustration)

REQUIRES: numpy, matplotlib (for animation)


THE MODEL

There is a 1 km one-lane road with traffic lights on the end (which
position and phases are fixed for some reason, maybe there're major
crossroads). Cars appearing at the beginning of the road every 2.2
seconds at average according to the poisson process. They drive along
at the maximum speed of 18 m/s, accelerating and decelerating when the
distance to a red light or car ahead increases or decreases. This
gives quite natural behavior qualitatively.


THE SIMULATION

Let's suppose this road was with traffic lights every 100m with stupid
phases and constantly jammed. Collect the stats run `./traffic.py >
traf1` (you can replace the last `for` cycle with `FuncAnimation`
invocation to see some animation; it's better to skip some time in the
fast `for` cycle and then enjoy the fully developed picture).

The quasy-steady state establishes in about 3000s, so we analyze the
stats after that moment (`./stats.py 3000 traf1`):

```
avg time to travel 177.075388968
avg speed 5.64731217493
cars passed 2828
avg cars per minute 24.2719002116
```

Quite slow. So stupid drivers will curse, protest and demand to
unmount all this obtrusive obstructions. Eventualy the city will yield
(comment the `lights.append` cycle in the middle of the source and
repeat):

```
avg time to travel 268.208205495
avg speed 3.72844670488
cars passed 2803
avg cars per minute 24.0573324941
```

34% degradation in time to travel and 1% in throughput. _(And, AARGH,
they will be happy with it, seeing the straight road and forgotten
that they used to ride it faster.)_


CONCLUSIONS

If somebody says that they should unmount traffic lights to speed up
dense traffic flow, tell him to take a hike.


Please, correct and improve
