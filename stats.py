#!/usr/bin/python

import sys
import numpy as np

t0 = float(sys.argv[1]) #end of the transient period
dt = np.loadtxt(sys.argv[2]) # assumes 'car_end_time SP car_start_time' format

dt = dt[dt[:,0] > t0]
ttt = dt[:,0]-dt[:,1] # time to travel

print "avg time to travel", ttt.mean()
print "avg speed", 1000.0/ttt.mean()
print "cars passed", len(ttt)
print "avg cars per minute", len(ttt)/(dt[-1,0]-t0)*60

print "\ntime to travel distribution"
tmh,tme = np.histogram(ttt)
for i in range(len(tmh)):
    print tme[i], tme[i+1], tmh[i]
